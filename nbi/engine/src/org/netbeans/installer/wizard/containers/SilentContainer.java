/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.netbeans.installer.wizard.containers;

import org.netbeans.installer.wizard.ui.WizardUi;

/**
 *
 * @author Dmitry Lipin
 */
public class SilentContainer implements WizardContainer {

    public void setVisible(boolean visible) {
        //do nothing
    }

    public void updateWizardUi(WizardUi ui) {
        //do nothing
    }
    
    public void open() {
        //do nothing
    }

    public void close() {
        //do nothing
    }
    
}
