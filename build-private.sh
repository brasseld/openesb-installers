#!/bin/bash -x

OUTPUT_DIR=$WORKSPACE/dist
mkdir -p $OUTPUT_DIR
CACHE_DIR=$WORKSPACE/cache
mkdir -p $CACHE_DIR

ANT_OPTS="-Xmx1024m -Djavac.target=1.6 -Djavac.source=1.6"

GLASSFISH_DOWNLOAD_SITE=http://dlc.sun.com.edgesuite.net/javaee5/v2.1.1_branch/promoted

OPENESB_CORE_DOWNLOAD_SITE=http://hudson.openesb-dev.org:8080/hudson/job/openesb-core-git
OPENESB_COMPONENTS_DOWNLOAD_SITE=http://hudson.openesb-dev.org:8080/hudson/view/dev-git/job/openesb-components-git
OPENESB_GLASSFISH_ADDONS_DOWNLOAD_SITE=http://hudson.openesb-dev.org:8080/hudson/view/dev-git/job/openesb-glassfish-addons-git
OPENESB_IDE_DOWNLOAD_SITE=http://hudson.openesb-dev.org:8080/hudson/view/dev-git/job/netbeans-soa-git


